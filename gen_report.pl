#!/usr/bin/perl

use lib '/homes/chuangw/cs354/spring2013/Text-CSV-1.21/lib/';
use Text::CSV;

my @rows;
my $csv = Text::CSV->new ( { binary => 1 }, {empty_is_undef => 1} )  # should set binary attribute.
or die "Cannot use CSV: ".Text::CSV->error_diag ();

open my $fh, "<:encoding(utf8)", "cs354lab1.csv" or die "cs354lab1.csv: $!";
my $caption = $csv->getline( $fh );
foreach (@{ $caption }){
  print "|$_|\t";
}
my $user_name = 1;
my $part3_1 = 2;
my $part3_1_comment = 3;
my $part3_2 = 4;
my $part3_2_comment = 5;
my $part3_3 = 6;
my $part3_3_comment = 7;
my $part3_4 = 8;
my $part3_4_comment = 9;
my $part4 = 10;
my $part4_1_comment = 11;
my $part4_2_comment = 12;
my $part5 = 13;
my $part5_comment = 14;
my $part6 = 15;
my $part6_comment = 16;
my $bonus = 21;
my $late_days = 22;
print "\n";

my $labid = "lab1";
while ( my $row = $csv->getline( $fh ) ) {
  my $s_name = trim(${ $row }[ $user_name ]);
  next if $s_name eq "";
  create_dir(  $s_name, $labid );
  gen_student_report( $row );
}
$csv->eof or $csv->error_diag();
close $fh;

sub create_dir{
  my $s_name = shift;
  my $labid = shift;
  unless( -d $s_name ){
    print "create directory for student '$s_name'\n";
    mkdir( $s_name );
  }
  #my $lab_path = "$s_name/$labid";
  #unless( -d $lab_path ){
  #  print "create report directory for student '$s_name', lab '$labid'\n";
  #  mkdir( $lab_path );
  #}
}

sub gen_student_report {
  my $row = shift;
  #$row->[2] =~ m/pattern/ or next; # 3rd field should match
  #push @rows, $row;
  #next if( not defined( ${ $row }[ $user_name ] ) );
  #next if ${ $row }[ $user_name ] == "" ;
  my $s_name = trim(${ $row }[ $user_name ]);

  my $raw_total = 
    ${ $row }[ $part3_1 ] + 
    ${ $row }[ $part3_2 ] + 
    ${ $row }[ $part3_3 ] + 
    ${ $row }[ $part3_4 ] + 
    ${ $row }[ $part4 ] + 
    ${ $row }[ $part5 ] + 
    ${ $row }[ $part6 ];

  my $bonus = (${ $row }[ $bonus ] eq "yes")?"yes":"no";
  print "$bonus\n";
  my $late_days = (${ $row }[ $late_days ] eq "")?"0":${ $row }[ $late_days ];

  my $student_total = $raw_total;
  if ( $bonus eq "yes" ){
    $student_total = int($raw_total * 1.1);
  }

  print ${ $row }[ $user_name ]  . " Total: " . $student_total . "\n";

  my $report_path = "$s_name/$labid.rpt";
  my $fh;
  open( $fh, ">", $report_path ) or die("can't open file $report_path to write");

  print $fh <<EOF;
${ $row }[ $user_name ]
Total: $student_total

Here's the breakdown:

Part3.1: ${ $row }[ $part3_1 ]
Comment: 
${ $row }[ $part3_1_comment ]

Part3.2: ${ $row }[ $part3_2 ]
Comment: 
${ $row }[ $part3_2_comment ]

Part3.3: ${ $row }[ $part3_3 ]
Comment: 
${ $row }[ $part3_3_comment ]

Part3.4: ${ $row }[ $part3_4 ]
Comment: 
${ $row }[ $part3_4_comment ]

Part4: ${ $row }[ $part4 ]
Comment: 
Part 4.1:
${ $row }[ $part4_1_comment ]
Part 4.2:
${ $row }[ $part4_2_comment ]

Part5: ${ $row }[ $part5 ]
Comment: 
${ $row }[ $part5_comment ]

Part6: ${ $row }[ $part6 ]
Comment: 
${ $row }[ $part6_comment ]

Raw Total:
$raw_total

Early Submission Bonus:
$bonus

Late Days Used:
$late_days

Inquries:
For part 3.1, 3.2 and 3.3, please send inquries to Duong Ngoc nduong\@purdue.edu
For part 3.4 and 4,        please send inquries to Wei-Chiu chuangw\@purdue.edu
For part 5 and 6,          please send inquries to Hou-Jen ko16\@purdue.edu
EOF

  close $fh;
}

sub trim($)
{
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
