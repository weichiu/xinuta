#!/usr/bin/perl

use lib '/homes/chuangw/cs354/spring2013/Text-CSV-1.21/lib/';
use Text::CSV;

my @rows;
my $csv = Text::CSV->new ( { binary => 1 }, {empty_is_undef => 1} )  # should set binary attribute.
or die "Cannot use CSV: ".Text::CSV->error_diag ();

open my $fh, "<:encoding(utf8)", "cs354lab3.csv" or die "cs354lab3.csv: $!";
my $caption = $csv->getline( $fh );
foreach (@{ $caption }){
  print "|$_|\t";
}
my $user_name = 0;

my $part1 = 1;
my $part1_discussion = 2;
my $part1_comment = 3;


my $part2_1 = 4;
my $part2_1_test1 = 5;
my $part2_1_test2 = 6;
my $part2_1_comment = 7;

my $part2_3 = 8;
my $part2_3_comment = 9;

my $late_days = 10;
my $raw_total = 11;
my $other_adjustments = 12;
my $adjusted = 13;
my $bonus = 14;
my $total = 15;
print "\n";

my $labid = "lab3";
while ( my $row = $csv->getline( $fh ) ) {
  my $s_name = trim(${ $row }[ $user_name ]);
  next if $s_name eq "";
  create_dir(  $s_name, $labid );
  gen_student_report( $row );
}
$csv->eof or $csv->error_diag();
close $fh;

sub create_dir{
  my $s_name = shift;
  my $labid = shift;
  unless( -d $s_name ){
    print "create directory for student '$s_name'\n";
    mkdir( $s_name );
  }
}

sub gen_student_report {
  my $row = shift;
  my $s_name = trim(${ $row }[ $user_name ]);

  my $report_path = "$s_name/$labid.rpt";
  my $fh;
  open( $fh, ">", $report_path ) or die("can't open file $report_path to write");

  print $fh <<EOF;
${ $row }[ $user_name ]
Total: ${ $row }[ $total ]

Here's the breakdown:

Part1: ${ $row }[ $part1 ]
Part1 Discussion : ${ $row }[ $part1_discussion ]
Comments: 
${ $row }[ $part1_comment ]

Part2.1: ${ $row }[ $part2_1 ]
Part2.1 test case 1: ${ $row }[ $part2_1_test1 ]
Part2.1 test case 2: ${ $row }[ $part2_1_test2 ]
Comments: 
${ $row }[ $part2_1_comment ]

Part2.3: ${ $row }[ $part2_3 ]
Comments: 
${ $row }[ $part2_3_comment ]


Raw Total:
${ $row }[ $raw_total ]

Other Adjustments:
${ $row }[ $other_adjustments ]

Early Submission Bonus:
${ $row }[ $bonus ]

Late Days Used:
${ $row }[ $late_days ]

Inquries:
For part 1, please send inquries to Wei-Chiu chuangw\@purdue.edu
The test case for part 1 can be downloaded at http://www.cs.purdue.edu/homes/chuangw/lab3_testcase.c
For part 2 approach 1, please send inquries to Hou-Jen ko16\@purdue.edu
For part 2 approach 3, please send inquries to Nguyen Ngoc nduong\@purdue.edu
EOF

  close $fh;
}

sub trim($)
{
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
