#!/usr/bin/perl

use lib '/homes/chuangw/cs354/spring2013/Text-CSV-1.21/lib/';
use Text::CSV;

my @rows;
my $csv = Text::CSV->new ( { binary => 1 }, {empty_is_undef => 1} )  # should set binary attribute.
or die "Cannot use CSV: ".Text::CSV->error_diag ();

open my $fh, "<:encoding(utf8)", "cs354lab4.csv" or die "cs354lab4.csv: $!";
my $caption = $csv->getline( $fh );
foreach (@{ $caption }){
  print "|$_|\t";
}
my $user_name = 0;

my $test1 = 1;

my $test2 = 2;
my $test3 = 3;
my $student_testcase = 4;

my $lab_answer = 5;

my $raw_total = 6;
my $late_days = 7;
my $other_adjustments = 8;
my $adjusted = 9;
my $bonus = 10;
my $total = 11;
print "\n";

my $labid = "lab4";
while ( my $row = $csv->getline( $fh ) ) {
  my $s_name = trim(${ $row }[ $user_name ]);
  next if $s_name eq "";
  create_dir(  $s_name, $labid );
  gen_student_report( $row );
}
$csv->eof or $csv->error_diag();
close $fh;

sub create_dir{
  my $s_name = shift;
  my $labid = shift;
  unless( -d $s_name ){
    print "create directory for student '$s_name'\n";
    mkdir( $s_name );
  }
}

sub gen_student_report {
  my $row = shift;
  my $s_name = trim(${ $row }[ $user_name ]);

  my $report_path = "$s_name/$labid.rpt";
  my $fh;
  open( $fh, ">", $report_path ) or die("can't open file $report_path to write");

  print $fh <<EOF;
${ $row }[ $user_name ]
Total: ${ $row }[ $total ]

Here's the breakdown:

test case1: ${ $row }[ $test1 ]

test case2: ${ $row }[ $test2 ]

test case3: ${ $row }[ $test3 ]

student test case: ${ $row }[ $student_testcase ]

LabAnswer.pdf: ${ $row }[ $lab_answer ]

Raw Total:
${ $row }[ $raw_total ]

Other Adjustments:
${ $row }[ $other_adjustments ]

Early Submission Bonus:
${ $row }[ $bonus ]

Late Days Used:
${ $row }[ $late_days ]

Inquries:
Please direct all inquries to Nguyen Ngoc nduong\@purdue.edu
EOF

  close $fh;
}

sub trim($)
{
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
