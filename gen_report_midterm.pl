#!/usr/bin/perl

use lib '/homes/chuangw/cs354/spring2013/Text-CSV-1.21/lib/';
use Text::CSV;

my @rows;
my $csv = Text::CSV->new ( { binary => 1 }, {empty_is_undef => 1} )  # should set binary attribute.
or die "Cannot use CSV: ".Text::CSV->error_diag ();

open my $fh, "<:encoding(utf8)", "cs354midterm.csv" or die "cs354midterm.csv: $!";
my $caption = $csv->getline( $fh );
foreach (@{ $caption }){
  print "|$_|\t";
}
my $user_name = 0;
my $lab1_score = 2;
my $midterm_score = 3;
my $lab2_score = 4;
my $lab3_score = 5;
print "\n";

my $labid = "midterm";
while ( my $row = $csv->getline( $fh ) ) {
  my $s_name = trim(${ $row }[ $user_name ]);
  next if $s_name eq "";
print "$s_name\n";
  create_dir(  $s_name, $labid );
  gen_student_report( $row );
}
$csv->eof or $csv->error_diag();
close $fh;

sub create_dir{
  my $s_name = shift;
  my $labid = shift;
  unless( -d $s_name ){
    print "create directory for student '$s_name'\n";
    mkdir( $s_name );
  }
  #my $lab_path = "$s_name/$labid";
  #unless( -d $lab_path ){
  #  print "create report directory for student '$s_name', lab '$labid'\n";
  #  mkdir( $lab_path );
  #}
}

sub gen_student_report {
  my $row = shift;
  #$row->[2] =~ m/pattern/ or next; # 3rd field should match
  #push @rows, $row;
  #next if( not defined( ${ $row }[ $user_name ] ) );
  #next if ${ $row }[ $user_name ] == "" ;
  my $s_name = trim(${ $row }[ $user_name ]);

  my $lab1_total = 
    ${ $row }[ $lab1_score ];
  my $midterm_total = 
    ${ $row }[ $midterm_score ];
  my $lab2_total = 
    ${ $row }[ $lab2_score ];
  my $lab3_total = 
    ${ $row }[ $lab3_score ];


  print ${ $row }[ $user_name ]  . " Total: " . $midterm_total . "\n";

  my $report_path = "$s_name/$labid.rpt";
  my $fh;
  open( $fh, ">", $report_path ) or die("can't open file $report_path to write");

  #print <<EOF;
  print $fh <<EOF;
${ $row }[ $user_name ]
Lab1: $lab1_total
Midterm: $midterm_total
Lab2: $lab2_total
Lab3: $lab3_total
EOF

  close $fh;
}

sub trim($)
{
  my $string = shift;
  $string =~ s/^\s+//;
  $string =~ s/\s+$//;
  return $string;
}
