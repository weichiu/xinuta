#!/bin/bash
basedir=`pwd`
echo $basedir

skipped=0

if [ $# -eq 1 ]
then
  echo "Skip until " $1
fi


for submission in *
do
	echo $submission
	#echo `expr match "$submission" '.*\.sh$' `
	if [ 0 -eq  `expr match "$submission" '.*\.sh$' ` ]
	then
    
    if [ $# -eq 1 ]
    then
      if [ $1 = $submission ]
      then
        echo "start from " $1
      else
        echo "skip" $1
        continue
      fi
    fi

    #if [ $# -eq 1 -a $skipped -eq 0 ]
    #then
    #  if [ $submission = $1 ]
    #  then
    #    skipped=1
    #  fi      
    #  echo "skipping " $submission
    #  continue
    #fi
    #bonus=`grep $submission bonus.txt | awk '{print $2}'`

    #extratest=`grep $submission extratest.txt | awk '{print $2}'`

    cd $basedir/$submission

    cd `ls -d xinu*`

    #cd `ls -d lab2*`

    echo "at " `pwd`

    echo "Pause before patching main.c"
    read choice

    cd compile/
    make
    ../../../../manual-run.exp xinu105 xinu


    # Ok.. grading
    #cp ../../rungrading.sh .

    #cp ../../../Makefile Makefile.grading

    #../../rungrading.sh $bonus $extratest 2>&1 | tee build.err

    #mkdir ../../../lab2-report/$submission
    #cp build.err ../../../lab2-report/$submission/grading-form.txt
    #make clean
    #make CC=g++ test1 test2 test3 test5 test6

    # done
		#cd ..
    #cd ..
    #cat ../lab2-report/grading-explanation >> ../lab2-report/$submission/grading-form.txt
	else
		echo "$submission's a shell script"
	fi 
done
