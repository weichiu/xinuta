#!/bin/bash

#################
# settings
# - the cs-status command
CSSTATUS=/p/xinu/bin/cs-status
# - where the logs will be put
DROPZONE=/u/u3/503/www/backend-status
#
#################


#################
# settings
# - the cs-status command
CSSTATUS=/p/xinu/bin/cs-status
# - where the logs will be put
DROPZONE=/u/u3/503/www/backend-status
#
#################

## snippet from tomcat startup script, to determine where this script is,
## based on the command line used to invoke it
# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
     PRG="$link"
  else
     PRG=`dirname "$PRG"`/"$link"
  fi
done
		  
whereami=`dirname "$PRG"`


backend_list=`$CSSTATUS | grep -v helga | awk '{print $1}' `


# initialize the score of each backend
nb_measures=0
for backend in $backend_list
do
    eval "$backend=0"
#    echo $backend $(eval echo \$$backend)
done

for i in $DROPZONE/*status
do
    nb_measures=`expr $nb_measures + 1`
    echo nb_measures = $nb_measures
    filename=`basename $i`
    split_name=`echo $filename | tr ':' ' ' | tr '-' ' ' `
    year=`echo $split_name | cut -d ' ' -f 1` 
    month=`echo $split_name | cut -d ' ' -f 2` 
    day=`echo $split_name | cut -d ' ' -f 3`

    for backend in $backend_list
    do
	status=`grep $backend $i | awk '{print $3}'`
#	echo year: $year month: $month day: $day backend: $backend status: $status
	# increase the count if the backend is working
	if [ "x$status" = "xworks" ]
	then
#	    echo backend $backend working
#	    echo $backend = expr $(eval echo \$$backend) + 1
#	    echo $backend = $(eval expr $(eval echo \$$backend) + 1)
	    eval "$backend=$(eval expr $(eval echo \$$backend) + 1)"
	fi
    done
done


echo \#\# Scores of the backends
for backend in $backend_list
do
    score=$(echo scale = 2 \; $(eval echo \$$backend) / $nb_measures | bc)
    echo $backend $score
done
