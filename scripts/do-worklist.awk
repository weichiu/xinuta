# awk script to execute a worklist, received on stdin, with the following
# format: 
# <login> <kernel> <logfile> <command to run>
#
# Where
# - <login> is the login of the student
# - <kernel> is the kernel to run
# - <logfile> is the file to which the output of the test should be logged
# - <command to run> is what should be sent to the shell prompt (for paging)
#
#
# TODO: 
#  - implement the switch that detects whether there is a command or not
#
#

BEGIN {
	/* where am I ? */
	
	whereami = "/homes/jthomas/private/scripts" 
	print "I am at "whereami


	/* some environmental definitions */
	log_wrapper_simple = "log_wrapper_simple.sh" ;
	
	/* initialize the list of machines */

/* 	machines[0] = "xinu131" */
/*	machines[1] = "xinu132" */
/*	machines[2] = "xinu141" */
/*	machines[3] = "xinu142" */
/*	machines[0] = "xinu144" */
/*	machines[1] = "xinu147" */
	machines[0] = "xinu149" 
	machines[1] = "xinu151" 
	machines[2] = "xinu159"
	machines[3] = "xinu160"
	machines[4] = "xinu161"
	machines[5] = "xinu168"
	machines[6] = "xinu171"

/* died: */
/*	machines[2] = "xinu135" */
/*	machines[11] = "xinu162" */

	nmachines = 7
	print "there are "nmachines" machines"

	/* randomly pick the first machine we will use */
	srand(); /* for real randomness */
	next_machine = int( rand() * nmachines )
	print "starting with machine # "next_machine" ("machines[next_machine]")"
}

/^#/   { /* skip comment lines that begin with a '#' sign */
	/* print "ignored line: "$0 */
	next
}

 { /* just run the kernel */

	parse_line()
	
	print "Simple run: "login" "kernel" "logfile
	
	need_retry = 1
	while(need_retry){
		machine = get_machine()
		#print whereami"/"log_wrapper_simple" "logfile" "machine" "kernel 
		need_retry = system( whereami"/"log_wrapper_simple" "logfile" "machine" "kernel" "$0 );
		release_machine()
	}
}


# stolen from httpd.awk
function shift(s)
{
	s = $1;
	gsub(/^[ \t]*[^ \t]*[ \t]*/,"",$0);
	return s;
}

# factoring parsing code
function parse_line(){

	login = shift();
	kernel = shift();
	logfile = shift();
	command = $0; /* rest of the line */

}

function get_machine(){
	/* eventually these functions will call a thread-safe allocator */
	/* so that the tests can be run in parallel on separate machines */
	ret = machines[ next_machine ]
	next_machine = ++next_machine % nmachines
	return ret
}

function release_machine(){
	return
}
