#!/bin/bash
#
# script to retrieve the picture from the CS webcam that monitors
# the construction of the new CS building
#

DATE=`date +"%F-%X"`
wget http://buildingcam.cs.purdue.edu/netcam.jpg -O /homes/jthomas/cs-construction/csbldg-${DATE}.jpg > /dev/null 2>&1
