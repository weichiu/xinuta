#! /bin/bash

comments="$(cat comments)"
student=$(basename `pwd`)


cat <<EOF

                CS 503 Lab0 grading sheet

Student Login: $student

You turned this lab in and it runs, so you get full credits for it.

Now here are some comments:


Programming Answers:

$comments

EOF
