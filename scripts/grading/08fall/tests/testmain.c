/* test cases for lab0*/

#include <conf.h>
#include <kernel.h>
/* #include <lab0.h> */

int prT();
int prT2();
main ()
{

  unsigned short shrt;
  unsigned long lng;
  int prA;
  int prB;
  int x = 1;
  int y = 2;
  int z = 3;
  int *addr;  

  /* For Question 1 */
  
  kprintf("\n QUESTION 1: Possible points 20 \n");
  
  lng = 0xaabbccdd;
  kprintf("\n NET2HL: In NetbyteOrder:%x In HostByteOrder: %x \n", lng, (unsigned long) net2hostl_asm(lng)); 
  
  lng = 0x0;
  kprintf("\n NET2HL: In NetbyteOrder:%x In HostByteOrder: %x \n", lng, (unsigned long) net2hostl_asm(lng)); 
  
  
  /* For Question 2 */
  
   kprintf("\n QUESTION 2: Possible point 20 \n");

   printsegaddress();  

  /* For Question 3 */

   kprintf("\n QUESTION 3: Possible point 20 \n");

   addr = &x; /* taking address inhibits const. prop. optimization */
   addr = &y;
   addr = &z;

   prT2(x,y,z);

  /* Question 4*/
  
  kprintf("\n QUESTION 4 : Possible point 20 \n");

   printprocstks();
   prA = create(prT, 1500, 15, "proc A", 1, 'A');
   resume(prA);
   prB = create(prT, 1800, 10, "proc B", 1, 'B');
   resume(prB);
   kprintf("After creating process A with pid : %d and B with pid %d \n\n", prA, prB);
   printprocstks();  

  kprintf("\n TESTING DONE \n");
}

prT(c)
char c;
{


}


unsigned int *esp;


void myprinttos()
   {
	   // My code for printing 
	   int i;
	   
	   kprintf("\nMY OWN stack printing\n");

	   asm("movl %esp, esp ;");

	   kprintf("top of stack (\"below\" location pointed to by esp)\n");
	   for(i=0; i<30; i++)
	   {
		   int *slot = (esp + i);
		   kprintf("@%08X (%8d): %08X (%8d) \n", 
				   slot, slot, *(slot), *(slot));
	   }

	

   }

int prT2(arg1, arg2, arg3)
int arg1;
int arg2;
int arg3;
{
   int a = 4;
   int b = 5;
   int c = 6;
   int *addr;

   a = arg1 + arg2 + arg3; /* use the args, dead value optimiztion */

   addr = &a; /* take address, avoid const prop. optimization */
   addr = &b;
   addr = &c;

   kprintf("printing stack within called process\n");
   printtos();
   myprinttos();

}

