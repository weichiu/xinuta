#! /bin/bash

source prog-grades-1
source prog-grades-2
source prog-grades-3
source prog-grades-4
source lab-grades
source hw-grades
student=$(basename `pwd`)


cat <<EOF

                CS 503 Lab0 grading sheet

Student Login: $student

Programming
-----------

Question 1 (20 pts): $prog1

Question 2 (20 pts): $prog2

Question 3 (20 pts): $prog3

Question 4 (20 pts): $prog4

Write-up
--------

Question 1 (10 pts): $lab1

Question 2 (10 pts): $lab2

Question 3 (10 pts): $lab3

Question 4 (10 pts): $lab4

Homework
--------

Question 1 (20 pts): $hw1

Question 2 (15 pts): $hw2

Question 3 (10 pts): $hw3

Question 4 (15 pts): $hw4



TOTAL:          $(expr $prog1 + $prog2 + $prog3 + $prog4 + $lab1 + $lab2 + $lab3 + $lab4 + $hw1 + $hw2 + $hw3 + $hw4)

EOF
