#!/bin/bash


OVERLAY=/homes/jthomas/grading/lab3/tests/overlay-new


echo please remember yo have to be in extract
echo and have the submissions at the same dir level

if [ $# -lt 1 ]
then
    echo you need to provide a student name
    exit 1
fi

student=$1

echo generating kernels for student $student

mkdir $student
cd $student
tar xzvf ../../submissions/${student}.Z || exit 1

# whatever their root dir is :-)
cd */compile || exit 1

# add our stuff in here
cp -r $OVERLAY/* ../

#fix NFRAMES to 3072
sed -i 's/#define[ \t]*NFRAMES.*/#define NFRAMES 3072/' ../h/paging.h

make || exit 1

cp ./xinu ../../xinu-1

#fix NFRAMES to 30
sed -i 's/#define[ \t]*NFRAMES.*/#define NFRAMES 30/' ../h/paging.h

make || exit 1

cp ./xinu ../../xinu-2

cd ../../

