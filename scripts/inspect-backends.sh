#!/bin/bash
#
# script to collect the health status of the Xinu backends
#

#################
# settings
# - the cs-status command
CSSTATUS=/p/xinu/bin/cs-status
# - where the logs will be put
DROPZONE=/u/u50/chuangw/.www/cs354/spring2013/backend-status
#
#################

## snippet from tomcat startup script, to determine where this script is,
## based on the command line used to invoke it
# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
     PRG="$link"
  else
     PRG=`dirname "$PRG"`/"$link"
  fi
done
		  
whereami=`dirname "$PRG"`

DATE_START=`date +"%F-%X"`
LOGFILE=$DROPZONE/$DATE_START-status
DETAILSDIR=$LOGFILE-details
CURRENT=$DROPZONE/_CURRENT

echo "#start: $DATE_START" >> $LOGFILE

if [ ! -d $DROPZONE ] ; then
	echo No directory to dump the logs
	exit -1
fi

touch $LOGFILE
chmod o+r $LOGFILE
mkdir $DETAILSDIR
chmod o+rx $DETAILSDIR
echo $LOGFILE
for backend in `$CSSTATUS | grep -v helga | awk '{print $1}' `; do

	$whereami/test-backend.sh -b $backend -t 45 -v 2>&1 > $DETAILSDIR/$backend
	tail -n 1 $DETAILSDIR/$backend >> $LOGFILE
	
done

DATE_END=`date +"%F-%X"`

echo "#done: $DATE_END" >> $LOGFILE

rm -f $CURRENT
ln -s $LOGFILE $CURRENT
