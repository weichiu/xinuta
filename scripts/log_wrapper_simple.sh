#!/bin/bash

## snippet from tomcat startup script, to determine where this script is,
## based on the command line used to invoke it
# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
     PRG="$link"
  else
     PRG=`dirname "$PRG"`/"$link"
  fi
done
            
whereami=`dirname "$PRG"`


# get the name of the logfile
LOGFILE=$1 ; shift


# invoke the expect script to automatically run the kernel, 
# based on the received arguments
${whereami}/run-kernel.exp $@ 2>&1 | tee $LOGFILE 2>&1 

# see if we failed (backend in use), so we need to try again
# on another machnine
need_retry=0;
tail -n 1 $LOGFILE | grep "not available" >/dev/null && need_retry=1
[ $need_retry -eq 1 ] && exit -1 || exit 0


