/*computes the maximum of the numbers received, one by line */
BEGIN{
    initialized = 0
}
{ 
    if(! initialized){
	max = $1
	initialized = 1
    }
	
    if($1 > max)
	max = $1
}
END{
    print max
}