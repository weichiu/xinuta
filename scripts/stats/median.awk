/* compute the median of the numbers received, one by line */
{
    numbers[i++]=$1
}
END{
    nb=i

    /* "When in doubt, use brute force": quickselect another time, if needed */

    /* bubble sort for now */
    for(i=0; i<nb; i++){
	did_swap = 0
	for(j=0; j< (nb - 1); j++){
	    if(numbers[j] > numbers[j+1]){
		temp = numbers[j]
		numbers[j] = numbers[j+1]
		numbers[j+1] = temp
		did_swap = 1
	    } 
	}
	if(! did_swap)
	    break
    }

    nb_is_odd = nb%2;
    if(nb_is_odd){
	print numbers[int(nb/2)]
    } else {
	print (numbers[nb/2 -1] + numbers[nb/2]) / 2
    }

}