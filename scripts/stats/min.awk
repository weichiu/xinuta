/*computes the minimum of the numbers received, one by line */
BEGIN{
    initialized = 0
}
{ 
    if(! initialized){
	min = $1
	initialized = 1
    }
	
    if($1 < min)
	min = $1
}
END{
    print min
}