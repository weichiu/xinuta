/*computes root mean square of the numbers received, one by line */
BEGIN{}
NF==1{ 
    sum_squares += $1*$1 
    nb++
    sum += $1
}
# http://en.wikipedia.org/wiki/Standard_deviation#Simplification_of_the_formula
END{
    #print nb " data points"
    mean=sum/nb
    #print "average:" mean
    std_dev = sqrt( (sum_squares / nb) - mean*mean)
    sd=sqrt((sum_squares-nb*mean*mean)/(nb-1.0))
    #print "root mean square: " std_dev
    print std_dev
}
