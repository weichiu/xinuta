#!/bin/bash

#
# Description: script to test whether a Xinu backend is booting or not
#
# Author: jthomas@cs.purdue.edu
#

# list of supported options
USAGE="$0 [-v] -b xinu_backend"
OPTSTRING="b:ht:v"

## snippet from tomcat startup script, to determine where this script is,
## based on the command line used to invoke it
# resolve links - $0 may be a softlink
PRG="$0"

while [ -h "$PRG" ] ; do
  ls=`ls -ld "$PRG"`
  link=`expr "$ls" : '.*-> \(.*\)$'`
  if expr "$link" : '.*/.*' > /dev/null; then
     PRG="$link"
  else
     PRG=`dirname "$PRG"`/"$link"
  fi
done
		    
whereami=`dirname "$PRG"`

print_help () {
echo "Usage: $USAGE"
echo "
-b               name of the xinu backend to test (e.g. xinu101)
-h               print this help
-v               turn on verbose mode: show all output from the console
-t               timeout before considering that the boot failed
"
}

# default behaviors
verbose="false"   # don't show the console output by default
timeout=30

while getopts "$OPTSTRING" "option" ; do
    case $option in
    	b)
	     backend="$OPTARG"
	;;
    	h)
	     print_help
	     exit
	;;
	t)
	     timeout="$OPTARG"
	;;
    	v)
	     verbose="true"
	;;
    esac
done



if [ -z "$backend" ]
then
    echo "you need to provide the name of a backend to test (e.g. xinu101)"
    print_help
    exit 1
fi

if [ ! -f $whereami/run-kernel.exp ]
then 
    echo "could not find run-kernel.exp in directory: $whereami"
    exit 1
fi

#if [ ! -d logs ]
#then 
#    echo creating directory to store the logs
#    mkdir logs
#fi

# temp log files (2nd copy?)
LOGS=/u/u50/chuangw/scripts/logs_2013Spring

if [ $verbose = "true" ]
then 
	$whereami/run-kernel.exp $backend $whereami/xinu-test-kernel $timeout 2>&1 | tee $LOGS/$backend
else
	$whereami/run-kernel.exp $backend $whereami/xinu-test-kernel $timeout > $LOGS/$backend 2>&1
fi


# generate the status message based on the log
status=failed
reason="unknown"

grep -q "started" $LOGS/$backend && status=works
grep -q "failed to boot Xinu from network" $LOGS/$backend && reason="BOOTP problem"
grep -q "No EtherExpress PRO/10" $LOGS/$backend && reason="unsupported network card"
grep -q "error: connection not found" $LOGS/$backend && reason="disabled backend"
grep -q "timed out waiting for the monitor" $LOGS/$backend && reason="no monitor"
grep -q "error: connection not available" $LOGS/$backend && reason="backend in use" # should actually not be an error


if [ $status = failed ] ; then
	echo $backend status: $status reason: $reason 
else
	echo $backend status: $status
fi

